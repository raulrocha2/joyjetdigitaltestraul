import { createStore, combineReducers, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { composeWithDevTools } from 'redux-devtools-extension'
import { 
    productListReducers, 
    productDetailsReducer, 
    updateProductReducer, 
    deleteProductReducer,
    createProductReducer,
    createProductPromotionReducer,
    
 } from './reducers/productReducers'
import { cartReducer } from './reducers/cartReducers'
import { 
    userLoginReducer, 
    userRegisterReducer, 
    userDetailReducer, 
    userUpdateProfileReducer,
    userListReducer,
    adminUpdateUserReducer,
    deleteUserReducer,   
} from './reducers/userReducers'
import { 
    orderCreateReducer, 
    orderDetailsReducer, 
    orderPayReducer, 
    orderListMyReducer, 
    orderListAdminReducer,
    orderDelivereReducer, 
} from './reducers/orderReducers'

const reducer = combineReducers({
    
    productList         : productListReducers,
    productDetails      : productDetailsReducer,
    productCreate       : createProductReducer,
    productUpdate       : updateProductReducer,
    productDelete       : deleteProductReducer,
    createPromotion     : createProductPromotionReducer,
    
    cart                : cartReducer,
    userLogin           : userLoginReducer,
    userRegister        : userRegisterReducer,
    userDetail          : userDetailReducer,
    userUpdateProfile   : userUpdateProfileReducer,
    userList            : userListReducer,
    updateUser          : adminUpdateUserReducer,
    deleteUser          : deleteUserReducer,

    orderCreate         : orderCreateReducer,
    orderDetails        : orderDetailsReducer,
    orderPay            : orderPayReducer,
    orderDeliver        : orderDelivereReducer,
    adminListOrder      : orderListAdminReducer,
    mylistOrder         : orderListMyReducer,
    

    

})
const cartItemsFromStorage = localStorage.getItem('cartItems') ?
    JSON.parse(localStorage.getItem('cartItems')) : []

const userInfoFromStorage = localStorage.getItem('userInfo') ?
    JSON.parse(localStorage.getItem('userInfo')) : null  

const shippingAddressFromStorage = localStorage.getItem('shippingAddress') ?

    JSON.parse(localStorage.getItem('shippingAddress')) : {}



const initialState = {
    cart: {
        cartItems: cartItemsFromStorage,
        shippingAddress: shippingAddressFromStorage,
    },
    userLogin: {userInfo: userInfoFromStorage},
    
}

const middleware = [thunk]

const store = createStore(reducer, initialState,
    composeWithDevTools(applyMiddleware(...middleware)))

export default store