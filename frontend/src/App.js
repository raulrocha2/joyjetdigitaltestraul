import { Container } from 'react-bootstrap';
import { HashRouter as Router, Route } from 'react-router-dom';
import Header from './components/Header';
import Footer from './components/Footer';
import HomeScreen from './screens/HomeScreen';
import ProductScreen from './screens/products/ProductScreen';
import CartScreen from './screens/orders/CartScreen';
import LoginScreen from './screens/users/LoginScreen';
import RegisterScreen from './screens/users/RegisterScreen';
import ProfileScreen from './screens/users/ProfileScreen';
import UserListScreen  from './screens/users/UserListScreen';
import EditUserScreen  from './screens/users/EditUserScreen';
import ShippingScreen from './screens/orders/ShippingScreen';
import PaymentScreen from './screens/orders/PaymentScreen';
import PlaceOrderScreen from './screens/orders/PlaceOrderScreen';
import OrderScreen from './screens/orders/OrderScreen';
import OrderListScreen from './screens/orders/OrderListSreen';
import ProductListScreen from './screens/products/ProductListScreen';
import ProductEditScreen from './screens/products/ProductEditScreen';


function App() {
  return (
    <Router>
      <Header/>
      <main className="py-3">
        <Container>
          <Route path='/' component={HomeScreen} exact/>
          <Route path='/login' component={LoginScreen}/>
          <Route path='/register' component={RegisterScreen}/>
          <Route path='/product/:id' component={ProductScreen} />
          <Route path='/admin/productlist' component={ProductListScreen} />
          <Route path='/admin/product/:id/edit' component={ProductEditScreen} />
          <Route path='/cart/:id?' component={CartScreen} />
          <Route path='/admin/orderlist' component={OrderListScreen} />
          <Route path='/profile' component={ProfileScreen}/>
          <Route path='/admin/user/:id/edit' component={EditUserScreen} />
          <Route path='/Shipping' component={ShippingScreen} />
          <Route path='/payment' component={PaymentScreen} />
          <Route path='/placeorder' component={PlaceOrderScreen} />
          <Route path='/order/:id' component={OrderScreen} />
          <Route path='/admin/userlist' component={UserListScreen} />
        
          
        
        </Container>
      </main>
      <Footer />
      
      
    </Router>
  );
}

export default App;
