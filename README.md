## HI GUYS, THANKS FOR INVITE I'M GLAD FOR THIS

I craeted one e-commerce "JOYJETSHOP", in this site you can login with admin and add new products, update and delete this products(MENU - ADMINISTRATOR - PROUDCTS), see your customer and update your emails, add with seller or delete(MENU - ADMINISTRATOR - USERS), see the orders of your customers and check with paid and delivered (MENU - ADMINISTRATOR - ORDERS).
To create update or delete new promotions you need of painel django admin/base/discountprice/ for take this, each promotion can calculated in percent or discount direct in price of product. 
By default your user is your e-mail of sing up, in Users-Site.txt have one Super User django, one user admin and one Customer.
Method if payment  I implemented the paypal in (frontend/src/screens/OrderScreen.js) line 43 you need add your Client-id of paypal.
The price of delivered if price total items is smaller 200 the price delivered is 20, if smaller of 400 price is 1 and if is more then 400 delivered is free.
Your customer can look your products, add in cart but for buy it he need sing up with your name, e-mail and password. After that He can buy whrever he want. In menu profile he can see your orders and status of each "Order ID, Date, Total, Paid, Deliverd".

---

## Getting started
Next, you'll clone or downloand this repository

1. Create virtualenv. 
2. Install requirements.txt.
3. run server and enjoy it.
4. The users admin and customer be in Users-Site.txt.

---
##Routes base/views/routes_view
 		'admin/', 
        
        #Routes API Products
        'api/products/', #Get all Products
        'api/products/create/', 
        'api/products/create-discount/',
        'api/products/upload-image/',
        'api/products/<str:pk>/', #Get product by ID
        'api/products/update/<str:pk>/',
        'api/products/delete/<str:pk>/',
       

        #Routes API Users
        'api/users/', #Get all users authentication ADMIN required 
        'api/users/register/',
        'api/users/login/',
        'api/users/profile/', 
        'api/users/profile/update/',
        'api/users/<str:pk>/', #Get User by ID 
        'api/users/delete/<str:pk>/',

        #Routes API Orders 
        'api/orders/add/',
        'api/orders/myorders/', 
        'api/orders/admin-orders/', 
        'api/orders/<str:pk>/', 
        'api/orders/<str:pk>/pay/', 
        'api/orders/<str:pk>/deliver/',
---


## Frontend React JS
1. frontend has the folder "build" with the files that running with django.
2. In frondend/src/actions where are  URLs of API ,  i'm using axios for work with API.
3. For edit frontend you need install nodejs and run React.
