from django.test import TestCase
from django.test.client import Client
from base.models import * 
from django.contrib.auth.models import User 
from model_mommy import mommy
from model_mommy.recipe import Recipe, foreign_key
from datetime import datetime

class test_base_models(TestCase):

    def setUp(self):


        user =  mommy.make('User',
            username='testone',
            email='testone@tes.com',
            password='123123',
            is_staff=True
        )
     
        product = mommy.make(
            'Product', user=user, name='first product'
        )
        
        discount_price = mommy.make(
            'DiscountPrice', product = product
       
        )
        
        order = mommy.make(
            'Order', user = user,
        
        )

        orderitem = mommy.make(
            'OrderItem', product = product, order = order,
        )

        shippingaddress = mommy.make(
            'ShippingAddress', order = order,
        
        )

  
    def test_base_is_created(self):

        user_exist = User.objects.filter(email__icontains='testone@tes.com')
        self.assertEqual(len(user_exist), 1)

        product_exist = Product.objects.filter(name__icontains='first')
        self.assertEqual(len(product_exist), 1)

        discount_exist = DiscountPrice.objects.filter(product=1)
        self.assertEqual(len(discount_exist), 1)

        order_exist = Order.objects.filter(pk=1)
        self.assertEqual(len(order_exist), 1)

        order_items_exist = OrderItem.objects.filter(order=1)
        self.assertEqual(len(order_items_exist), 1)

        shippingaddress_exist = OrderItem.objects.filter(order=1)
        self.assertEqual(len(shippingaddress_exist), 1)