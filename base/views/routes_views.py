from django.shortcuts import render
from rest_framework.response import Response
from rest_framework.decorators import api_view

@api_view(['GET'])
def getRoutes(request):

    routes = [
    
        'admin/', 
        
        #Routes API Products
        'api/products/', #Get all Products
        'api/products/create/', 
        'api/products/create-discount/',
        'api/products/upload-image/',
        'api/products/<str:pk>/', #Get product by ID
        'api/products/update/<str:pk>/',
        'api/products/delete/<str:pk>/',
       

        #Routes API Users
        'api/users/', #Get all users authentication ADMIN required 
        'api/users/register/',
        'api/users/login/',
        'api/users/profile/', 
        'api/users/profile/update/',
        'api/users/<str:pk>/', #Get User by ID 
        'api/users/delete/<str:pk>/',

        #Routes API Orders 
        'api/orders/add/',
        'api/orders/myorders/', 
        'api/orders/admin-orders/', 
        'api/orders/<str:pk>/', 
        'api/orders/<str:pk>/pay/', 
        'api/orders/<str:pk>/deliver/',
    ]

    return Response(routes)