from django.shortcuts import render
from rest_framework.response import Response
from base.models import Product, DiscountPrice
from base.serializers import ProductSerializer, DiscountPriceSerializer
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger



@api_view(['GET'])
def getProducts(request):
    query = request.query_params.get('keyword')
    if query == None:
        query = ''

    products = Product.objects.filter(name__icontains=query)

    page = request.query_params.get('page')
    paginator = Paginator(products, 10)

    try:
        products = paginator.page(page)
    except PageNotAnInteger:
        products = paginator.page(1)
    except EmptyPage:
        products = paginator.page(paginator.num_pages)

    if page == None:
        page = 1

    
    serializer = ProductSerializer(products, many=True)
    return Response({'products': serializer.data, 'page': page, 'pages': paginator.num_pages})



@api_view(['GET'])
def getProductbyId(request, pk):
    product = Product.objects.get(_id=pk)
    
    #Get Product With discount 
    if DiscountPrice.objects.filter(product=product):
        product_to_discount = product.discountprice

        if product_to_discount.isPercent:
            
            percentage = (product.price * product_to_discount.value_discount) / 100
            price_promo = product.price - percentage

        else:
            price_promo = product.price - product_to_discount.value_discount

        product_promo = {
            '_id': product._id,
            'name': product.name,
            'image': product.image.url,
            'brand': product.brand,
            'category': product.category,
            'description': product.description,
            'price': price_promo,
            'countInStock': product.countInStock,
            'createdAt': product.createdAt,
            'oldprice': product.price
        }
        

        return Response(product_promo)
       
    serializer = ProductSerializer(product, many=False)
    
    return Response(serializer.data)


@api_view(['POST'])
@permission_classes([IsAdminUser])
def createProduct(request):
    user = request.user
    product = Product.objects.create(
        user = user,
        name = 'NAME',
        price = 0,
        brand = 'BRAND',
        countInStock = 0,
        category = 'CATEGORY',
        description = '',

    )

    
    serializer = ProductSerializer(product, many=False)
    
    return Response(serializer.data)


@api_view(['PUT'])
@permission_classes([IsAdminUser])
def updateProduct(request, pk):

    data = request.data
    product = Product.objects.get(_id=pk)
    
    product.name = data['name']
    product.price = data['price']
    product.brand = data['brand']
    product.category = data['category']
    product.description = data['description']
    product.countInStock = data['countInStock']
    product.save()
    
    serializer = ProductSerializer(product, many=False)
    return Response(serializer.data)


@api_view(['DELETE'])
@permission_classes([IsAdminUser])
def deleteProduct(request, pk):
    product = Product.objects.get(_id=pk)
    product.delete()
    return Response('Product was Deleted ')    


@api_view(['POST'])
@permission_classes([IsAdminUser])
def uploadImage(request):
    data = request.data

    product_id = data['product_id']
    product = Product.objects.get(_id=product_id)

    product.image = request.FILES.get('image')
    product.save()

    return Response('Image was upload')

@api_view(['POST'])
@permission_classes([IsAdminUser])
def createDiscountProducts(request):
    discount_products = DiscountPrice.objects.all()
    serializer = ProductSerializer(discount_products, many=True)
    return Response(serializer.data)