# Generated by Django 3.1.4 on 2021-04-28 13:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0002_auto_20210428_1204'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='product',
            name='numReviews',
        ),
        migrations.RemoveField(
            model_name='product',
            name='rating',
        ),
        migrations.DeleteModel(
            name='Review',
        ),
    ]
