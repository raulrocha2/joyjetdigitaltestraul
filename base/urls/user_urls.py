from django.urls import path
from base.views import user_views as views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
   
)

urlpatterns = [
    path('', views.getUsers, name="get_users"),
   
    path('register/', views.registerUser, name='register_users'),
    path('login/', views.MyTokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('profile/', views.getUserProfile, name="user_profile"),
    path('profile/update/', views.UpdateUserProfile, name="user_profile_update"),
    path('<str:pk>/', views.getUserById, name="get_user_by_id"),
    path('update/<str:pk>/', views.updateUserAdmin, name="update_user_admin"),
    path('delete/<str:pk>/', views.deleteUser, name="delete_user"),
    
]