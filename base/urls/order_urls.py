from django.urls import path
from base.views import order_views as views
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
   
)

urlpatterns = [
    
    path('add/', views.addOrderItems, name='add_order_items'),
    path('myorders/', views.getMyOrders, name='my_orders'),
    path('admin-orders/', views.getOrders, name='admin_orders'),
    path('<str:pk>/', views.getOrderById, name='user_order_by_id'),
    path('<str:pk>/pay/', views.updateOrderToPaid, name='pay'),
    path('<str:pk>/deliver/', views.updateOrderToDelivered, name='order_deliver'),
    
    
]