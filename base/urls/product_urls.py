from django.urls import path
from base.views import product_views as views

urlpatterns = [
    
    path('', views.getProducts, name="products"),
    path('create/', views.createProduct, name="create_product"),
    path('upload-image/', views.uploadImage, name="image_upload"),
    path('create-discount/', views.createDiscountProducts, name="create-discount"),
    path('<str:pk>/', views.getProductbyId, name="product_id"),
    path('update/<str:pk>/', views.updateProduct, name="update_product"),
    path('delete/<str:pk>/', views.deleteProduct, name="delete_product"),

    
]